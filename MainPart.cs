﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterSimulator
{
    class MainPart
    {
        /// <summary>
        /// Метод Main является входной точкой работы программы
        /// </summary>
        static void Main()
        {
            Printer printer = new Printer();

            Simulator simulator = new Simulator();

            simulator.SimulatorInitalParametrs();

            printer.PrinterInitialParametrs();
            
            simulator.PrintProcess();
        }
    }
}
