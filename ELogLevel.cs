﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterSimulator
{
    [Flags]
    public enum ELogLevel
    {
        None = 0,
        Printer = 1,
        Simulator = 2
    }
}
