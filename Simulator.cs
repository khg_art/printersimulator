﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PrinterSimulator
{
    public class Simulator : Log
    {
        public bool PrinterStatus { get; private set; }
        
        public int ReadLogLevel { get; private set; }

        public readonly int numberTicks = 20000;                     // Максимальное количество счета в цикле for
        public readonly int numberWarrantyPrintedPages = 8000;       // Гарантийное число страниц для печати принтером
        public readonly int taskMin = 10;                            // Минимальное кол-во страниц для новой задачи
        public readonly int taskMax = 40;                            // Максимальное кол-во страниц для новой задачи
        int taskSum = default(int);                                  // Количество напечатанных задач 

        public readonly double probabilityToAddTask = 30;            // Вероятность % добавления новой задачи
        public readonly double probabilityPaperStuck = 0.01;         // Вероятность % заедания бумаги
        public readonly double probabilityWarrantyCrash = 0;         // Вероятность % гарантийной поломки принтера
        public readonly double probabilityCrash = 0.01;              // Вероятность % поломки принтера после истечения гарантии
               
        public void SimulatorInitalParametrs()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(
                "#############################################################\n" +
                "#                   Parametrs of simulator                  #\n" +
                "#############################################################"
                );
            Console.ResetColor();

            Console.WriteLine(
                $"Probability of a new task: {probabilityToAddTask} %\n" +
                $"Probability of paper stuck: {probabilityPaperStuck} %\n" +
                $"Probability of printer crush up to {numberWarrantyPrintedPages} printed pages: {probabilityWarrantyCrash} %\n" +
                $"Probability of printer crush after {numberWarrantyPrintedPages} printed pages: {probabilityCrash} %\n" +
                $"Number of pages in new task: {taskMin} - {taskMax}\n"
                );
        }

        public void SelectLogLevel()
        {            
            Console.WriteLine(
                "#######################################\n" +
                "#              Log level              #\n" +
                "#######################################"
                );

            Console.Write("1 -> ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Printer");
            Console.ResetColor();

            Console.Write("2 -> ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Simulator");
            Console.ResetColor();

            Console.Write("3 -> ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Printer");
            Console.ResetColor();

            Console.Write(" + ");

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Simulator");
            Console.ResetColor();

            Console.Write("\nSelect log level: ");

            ReadLogLevel = Int32.Parse(Console.ReadLine());

            Console.Clear();
        }

        /// <summary>
        /// Метод PrintProcess осуществляет имитацию работы принтера, а именно:
        /// - добавление новой задачи для печати с определенной вероятностью (probabilityToAddTask)
        /// - осуществляет заедание бумаги в принтере во время печати с определенной вероятностью (probabilityPaperStuck)
        /// - случайная поломка принтера во время печати с определенной вероятностью (probabilityWarrantyCrash, probabilityCrash)
        /// </summary>
        public void PrintProcess()
        {        
            var tasks = new Queue<int>();

            var random = new Random();

            var printer = new Printer();

            PrinterStatus = true;

            SelectLogLevel();

            for (int tick = 0; tick < numberTicks; tick++)
            {   
                if (PrinterStatus == true)
                {
                    // Условие поломки принтера
                    if (printer.SumPrintedPages < numberWarrantyPrintedPages)
                    {
                        if (Probability(probabilityWarrantyCrash)) { PrinterStatus = false; }
                    }
                    else
                    {
                        if (Probability(probabilityCrash)) { PrinterStatus = false; }
                    }

                    var taskPages = random.Next(taskMin, taskMax);

                    // Условие появления новой задачи
                    if (Probability(probabilityToAddTask))
                    {
                        tasks.Enqueue(taskPages);

                        msgSimulator = $"New Task #{++taskSum} added:>  {taskPages} pages to print in queue {tasks.Count}";                       

                        Message(tick, msgSimulator, ReadLogLevel);
                    }

                    // Условие начала печати новой задачи
                    if (printer.ReadyToPrint && tasks.Any())
                    {
                        printer.Start(tick, tasks.Dequeue(), ReadLogLevel);
                    }

                    // Условие на проверку наличия бумаги в лотке перед печатью
                    if (printer.pagesInTray != 0)
                    {
                        printer.Print(tick, ReadLogLevel);
                    }

                    // Условие заедания бумаги в принтере
                    if (Probability(probabilityPaperStuck))
                    {
                        printer.ReadyTray = false;
                    }

                    printer.Tray(tick, ReadLogLevel);

                    /// <summary>
                    /// Метод выполняет срабатывание случайного события с определенной вероятностью
                    /// <param name="p">входное число, определяющая вероятность в %</param>
                    /// </summary>
                    bool Probability(double p)
                    {
                        bool prb = Convert.ToDouble(random.Next(10000)) / 100 < p;
                        return prb;
                    }

                    if (tick == numberTicks - 1)
                    {
                        msgSimulator = "Tick is over";

                        Message(tick, msgSimulator, ReadLogLevel);

                        Console.ReadKey();

                        Environment.Exit(0);
                    }
                }
                else
                {
                    msgSimulator = $"This printer is broken, need to service...";

                    Message(tick, msgSimulator, ReadLogLevel);

                    Console.ReadKey();

                    Environment.Exit(0);
                }      
            }        
        }              
    }
}
