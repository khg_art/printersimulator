﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PrinterSimulator
{
    public class Log
    {
        protected string msgNewTask { get; set; }

        protected string msgPrint { get; set; }

        protected string msgTray { get; set; }   

        public string msgSimulator { get; set; }

        public void Message(int tick, string msg, int readLogLevel)
        {
            var logLevel = ELogLevel.None;

            if (readLogLevel == 1) { logLevel = ELogLevel.Printer; }
            else if (readLogLevel == 2) { logLevel = ELogLevel.Simulator; }
            else if (readLogLevel == 3) { logLevel = ELogLevel.Printer | ELogLevel.Simulator; }
            else
            {
                Console.WriteLine("Uknown log level...");

                Thread.Sleep(3000);

                Environment.Exit(0);
            }
            
            if (logLevel.HasFlag(ELogLevel.Printer))
            {
                if (msg == msgNewTask)
                {
                    Console.Write($"[{DateTime.Now.ToString("HH:mm:ss")}] ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(msg);
                    Console.ResetColor();
                }

                if (msg == msgPrint)
                {
                    Console.Write($"[{DateTime.Now.ToString("HH:mm:ss")}] ");
                    Console.WriteLine(msg);
                }

                if (msg == msgTray)
                {
                    Console.Write($"[{DateTime.Now.ToString("HH:mm:ss")}] ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(msg);
                    Console.ResetColor();
                }
            }

            if (logLevel.HasFlag(ELogLevel.Simulator))
            {
                if (msg == msgSimulator)
                {
                    Console.Write($"[{DateTime.Now.ToString("HH:mm:ss")}] ");
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(msg);
                    Console.ResetColor();
                }
            }
        }        
    }    
}
