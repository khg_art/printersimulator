﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PrinterSimulator
{
    public class Printer : Simulator
    {
        public bool ReadyToPrint { get; set; }

        public bool ReadyTray { get; set; }

        public int Task { get; private set; }        

        public int PrintedPages { get; private set; }        

        public int SumPrintedPages { get; private set; }

        public int pagesInTray = new Random().Next(0, 150);

        public readonly int pagesMinInTray = 50;

        public readonly int pagesMaxInTray = 150;

        public readonly int timeDelay = 300;              

        int taskCount = default(int);

        int cnt = default(int);        
                
        public void PrinterInitialParametrs()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(
                "#######################################\n" +
                "#         Parametrs of printer        #\n" +
                "#######################################"
                );
            Console.ResetColor();

            Console.WriteLine(
                $"The initial number of pages in tray: {pagesInTray}\n" +
                $"Adding paper to tray: {pagesMinInTray} - {pagesMaxInTray}\n" +
                $"Time adding paper to tray: {5*timeDelay/1000} sec\n" +
                $"Speed printing: {Math.Round((double)((1000/timeDelay)*60))} pages/min\n"
                );
        }
        
        /// <summary>
        /// Метод осуществляет изменение статуса принтера к готовности на печать
        /// </summary>
        public Printer()
        {
            ReadyToPrint = true;

            ReadyTray = true;

            Task = 0;
        }

        /// <summary>
        /// Метод Start формирует сообщение (msgNewTask) о появлении новой задачи для печати
        /// </summary>
        public void Start(int tick, int pages, int ReadLogLevel)
        {
            ReadyToPrint = false;

            Task = pages;
            
            msgNewTask = $"Task #{++taskCount}:>  {pages} pages to print";

            Message(tick, msgNewTask, ReadLogLevel);
        }

        /// <summary>
        /// Метод формирует сообщение с информацией когда принтер печает, а именно формирует:
        /// - сообщение если нету задачи на печать в очереди
        /// - сообщение с кол-вом напечатанных страниц в данной задаче
        /// - сообщение если заела бумага в принтере
        /// </summary>
        public void Print(int tick, int ReadLogLevel)
        {
            if (Task == 0)
            {
                msgPrint = $"No Task";

                Message(tick, msgPrint, ReadLogLevel);

                Thread.Sleep(timeDelay);

                return;
            }

            if (PrintedPages++ < Task && pagesInTray >= 0)
            {
                if (ReadyTray)
                {
                    SumPrintedPages++;

                    pagesInTray--;

                    msgPrint = $"Printed page:>  {PrintedPages:00} from {Task:00}";

                    Message(tick, msgPrint, ReadLogLevel);

                    Thread.Sleep(timeDelay);
                }
                else
                {
                    msgTray = $"Paper stucked! Fix it [Y/N]? ";

                    Message(tick, msgTray, ReadLogLevel);

                    string answer = Console.ReadLine();

                    int tries = 1;

                    while(tries < 4)
                    {
                        if (answer == "Y" || answer == "y")
                        {
                            ReadyTray = true;

                            PrintedPages--;                           

                            break;
                        }
                        else if (answer == "N" || answer == "n")
                        {
                            msgTray = $"Auto exit, after 5 seconds...";

                            Message(tick, msgTray, ReadLogLevel);
                            
                            Thread.Sleep(5000);

                            Environment.Exit(0);
                        }
                        else
                        {
                            if (tries == 3) { Environment.Exit(0); }

                            Message(tick, msgTray, ReadLogLevel);

                            answer = Console.ReadLine();

                            tries++;
                        }                        
                    }                    
                }                
            }

            if (PrintedPages == Task)
            {
                PrintedPages = 0;

                Task = 0;

                ReadyToPrint = true;
            }           
        }

        /// <summary>
        /// Метод реализует имитацию лотка в принтере, формирует:
        /// - сообщение если бумага закончилась в лотке принтера
        /// - сообщение о добавлении бумаги в лоток принтера
        /// - сообщение о точном количестве добавленных страниц в лоток
        /// </summary>
        public void Tray(int tick, int ReadLogLevel)
        {            
            if (pagesInTray == 0)
            {
                if (cnt == 5)
                {   
                    pagesInTray = new Random().Next(pagesMinInTray, pagesMaxInTray); //Добавление листов в лоток

                    msgTray = $"Adding pages in tray -> {cnt*20}% \n";

                    Message(tick, msgTray, ReadLogLevel);

                    msgTray = $"{pagesInTray} pages added in tray\n";

                    Message(tick, msgTray, ReadLogLevel);

                    cnt = 0;

                    Thread.Sleep(timeDelay);
                }
                else
                {     
                    if (cnt == 0)
                    {
                        msgTray = $"WARNING:>  Printer tray is empty!\n";

                        Message(tick, msgTray, ReadLogLevel);
                    }               

                    msgTray = $"Adding pages in tray -> {cnt*20}%\n";

                    Message(tick, msgTray, ReadLogLevel);

                    cnt++;

                    Thread.Sleep(timeDelay);
                }         
            }
        }  
    }
}
